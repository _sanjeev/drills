const fs = require('fs');
// const cb = require ('./test/testproblem2.js');

const problem1 = (url, cb) => {
    fs.readFile (url, "utf-8", (err, data) => {
        if (err) {
            console.log (err);
        }else {
            cb(data, url);
        }
    })
}

const problem2 = (url, cb) => {
    fs.readFile (url, "utf-8", (err, data) => {
        if (err) {
            console.log (err);
        }else {
            const newData = data.toUpperCase();
            const fileName = 'one.txt';
            fs.writeFileSync (fileName, newData, (err) => {
                if (err) {
                    console.log (err);
                }
            })
            cb (newData, fileName);
            fs.writeFileSync ('filenames.txt', fileName + ' ', (err) => {
                if (err) {
                    console.log (err);
                }
            })
        }
    })
}

const problem3 = (url, cb) => {
    fs.readFile (url, "utf-8", (err, data) => {
        if (err) {
            console.log (err);
        }else {
            const newData = data.toLowerCase();
            const text = newData.split ('.');
            const fileName = 'two.txt'
            fs.writeFileSync (fileName, text.toString(), (err) => {
                if (err) {
                    console.log (err);
                }
            })
            cb (text.toString(), fileName);
            fs.appendFile ('filenames.txt', fileName + ' ', (err) => {
                if (err) {
                    console.log (err);
                }
            })
        }
    })
}

const problem4 = (url, cb) => {
    fs.readFile (url, "utf-8", (err, data) => {
        if (err) {
            console.log (err);
        }else {
            const newData = data.split (' ');
            newData.sort ();
            const fileName = 'three.txt';
            fs.writeFileSync (fileName, newData.toString(), (err) => {
                if (err) {
                    console.log (err);
                }
            })
            cb (newData.toString(), fileName);
            fs.appendFile ('filenames.txt', fileName + ' ', (err) => {
                if (err) {
                    console.log (err);
                }
            })
        }
    })
}

const problem5 = (url, cb) => {
    fs.readFile (url, "utf-8", (err, data) => {
        if (err) {
            console.log (err);
        }else {
            cb (data, url);
            const newData = data.split (' ');
            newData.forEach ((ele) => {
                    fs.unlink (ele, (err) => {
                        if (err) {
                            // console.log (err);
                        }
                    })
            })
        }
    })
}


module.exports = {problem1, problem2, problem3, problem4, problem5};