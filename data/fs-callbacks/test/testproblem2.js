const problem1 = require ('../problem2.js');
const problem2 = require ('../problem2.js');
const problem3 = require ('../problem2.js');
const problem4 = require ('../problem2.js');
const problem5 = require ('../problem2.js');
const fs = require ('fs');


const cb = (expected, path) => {
    const actual = fs.readFileSync (path, "utf-8");
    if (actual === expected) {
        console.log ("same");
    }else {
        console.log ("different");
    }
}



setTimeout (() => {
    problem1.problem1 ('./../data/fs-callbacks/lipsum.txt', cb);
}, 0)
setTimeout (() => {
    problem2.problem2 ('./../data/fs-callbacks/lipsum.txt', cb);
}, 1000)
setTimeout (() => {
    problem3.problem3 ('./one.txt', cb);
}, 2000)
setTimeout (() => {
    problem4.problem4 ('./two.txt', cb);
}, 3000)
setTimeout (() => {
    problem5.problem5('filenames.txt', cb);
}, 5000)


// problem5.problem5 ('filenames.txt', cb);
