const boards = require ('../../trello-callbacks/boards.json');
const createDeleteDirectory = require ('../problem1.js');
const cards = require ('../../trello-callbacks/cards.json');
const lists = require ('../../trello-callbacks/lists.json');

const res = [boards, cards, lists];

const dir = './../random';
const path = './../random/random.json'
const expected = dir;

res.forEach ((data) => {
    const actual = createDeleteDirectory (data, dir, path);
    if (expected === actual) {
        console.log ('same');
    }else {
        console.log ('different');
    }
})