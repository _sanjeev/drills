const fs = require('fs');


const createDeleteDirectory = (url, dir, path) => {

    const directory = dir;
    const filePath = path;
    if (fs.existsSync(directory)) {
        if (fs.existsSync(filePath)) {
            fs.unlink(filePath, (err) => {
                if (err) {
                    console.log (err);
                }
            })
            
        } else {
            fs.writeFileSync(filePath, JSON.stringify(url), "utf-8", (err) => {
                if (err) {
                    console.log (err);
                }
            })
        }
    } else {
        fs.mkdir(directory, (err) => {
            if (err) {
                console.log (err);
            }

            if (fs.existsSync(filePath)) {
                console.log('Exist');
            } else {
                // console.log ('Not exist');
                fs.writeFileSync(filePath, JSON.stringify(url), "utf-8", (err) => {
                    console.log(err);
                })
            }
        })
    }

    return directory;
}

module.exports = createDeleteDirectory;