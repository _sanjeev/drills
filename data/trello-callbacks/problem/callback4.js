const lists = require('./../lists.json');
const cards = require('./../cards.json');

const listId = Object.keys(lists);
const cardsId = Object.keys(cards);
let output = [];

const res = ((id) => {
    setTimeout(() => {
        listId.forEach((val) => {
            if (val === id) {
                let mindId = null;
                if (lists[val][0].name === 'Mind') {
                    mindId = lists[val][0].id;
                }
                cardsId.forEach((val) => {
                    if (val === mindId) {
                        output.push(cards[val]);
                    }
                })
            }
            console.log(output);
        })
    }, 2 * 1000)
})



module.exports = res;

