const res = require('./../callback3.js');
const callback = (data) => {
    if (data.length > 0) {
        console.log (data);
    } else {
        console.log('No data found');
    }
    let expected = [
        [
            {
                id: '1',
                description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
            },
            {
                id: '2',
                description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
            },
            {
                id: '3',
                description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
            }
        ]
    ];
    expected = JSON.stringify (expected.flat());
    data = JSON.stringify (data.flat())
    if (expected === data) {
        console.log ('same');
    }else {
        console.log ('different');
    }
}

res('qwsa221', callback);