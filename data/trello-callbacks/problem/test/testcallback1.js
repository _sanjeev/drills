const res = require('./../callback1.js');

const callback = ((actual) => {
    if (actual.length > 0) {
        console.log(actual);
    } else {
        console.log('cannot find the data');
    }
    const expected = [{ id: 'mcu453ed', name: 'Thanos', permissions: {} }];
    if (actual.toString() === expected.toString()) {
        console.log ('same');
    }else {
        console.log ('different');
    }
})

res('mcu453ed', callback);