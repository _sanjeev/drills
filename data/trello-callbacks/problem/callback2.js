const list = require('./../lists.json');

const data = Object.keys(list);
// console.log (data);
const callback2 = ((boardId, callback) => {
    setTimeout(() => {
        let output = [];
        data.forEach((val) => {
            if (val === boardId) {
                output.push(list[val]);
            }
        })
        // console.log (output);
        callback(output);
    }, 2000)
})

module.exports = callback2;