const boards = './../../boards.json';
const fs = require ('fs');

function res (boardId, callback) {
    setTimeout (() => {
        const data = fs.readFileSync (boards, "utf-8");
        const res = JSON.parse (data);
        let output = [];
        res.forEach ((val) => {
            if (val.id === boardId) {
                output.push (val);
            }
        })
        callback (output);
    }, 2000);
}

module.exports = res;
