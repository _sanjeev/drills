const cards = require('./../cards.json');
const cardsId = Object.keys(cards);

const res = ((id, callback) => {
    setTimeout(() => {
        let output = [];
        cardsId.forEach((val) => {
            if (val === id) {
                output.push(cards[val]);
            }
        })
        callback(output);
    }, 2000)
})

module.exports = res;